from django.shortcuts import redirect, render

def home(request):
    return render(request, 'home.html')

def get_sum(request):
    numbers = request.GET['sum']
    both = numbers.split()
    first = int(both[0])
    second = int(both[1])
    total = first+second
    return render(request, 'home.html', {
        'first':first,
        'second':second,
        'sum': total
    })